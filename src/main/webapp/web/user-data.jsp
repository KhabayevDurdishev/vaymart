<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>User information</title>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/web/css/bootstrap.min.css">
    <link rel="stylesheet" href="/web/css/owl.carousel.css">
    <link rel="stylesheet" href="/web/css/font-awesome.min.css">
    <link rel="stylesheet" href="/web/css/responsive.css">
    <link rel="stylesheet" href="/web/css/style.css">

</head>
<body>
<div class="header-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="user-menu">
                    <ul>
                        <li><a href="/webui/index"><i class="fa fa-user"></i> Main Menu</a></li>
                        <li><a href="/webui/account/"><i class="fa fa-user"></i> Account</a></li>
                        <li><a href="/webui/cart"><i class="fa fa-user"></i> My Cart</a></li>
                        <li><a href="/webui/logout"><i class="fa fa-user"></i> Logout</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-4">
                <div class="header-right">
                    <ul class="list-unstyled list-inline">
                        <li class="dropdown dropdown-small">
                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">currency :</span><span class="value">USD </span><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">USD</a></li>
                            </ul>
                        </li>

                        <li class="dropdown dropdown-small">
                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">language :</span><span class="value">English </span><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">English</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="site-branding-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="logo">
                    <h1><a href="/webui/index">VAYMART</a></h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="product-big-title-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-bit-title text-center">
                    <h2>User information</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="single-product-area">
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-8">
                <div class="product-content-right">
                    <div class="woocommerce">
                        <form action="/webui/account/user-information" method="POST">
                            <div id="customer_details" class="col2-set">
                                <div class="col-1">
                                    <p class="woocommerce-billing-fields">
                                    <p class="form-row form-row-first validate-required">
                                        <label for="billing_first_name">First Name <abbr title="required" class="required"></abbr></label>
                                        <input type="text" id="billing_first_name" name="name" class="input-text" required>
                                    </p>

                                    <p class="form-row form-row-last validate-required">
                                        <label for="billing_last_name">Last Name <abbr title="required" class="required"></abbr></label>
                                        <input type="text" id="billing_last_name" name="surname" class="input-text" required>
                                    </p>

                                    <p class="form-row form-row-wide address-field validate-required">
                                        <label for="billing_city">Town / City <abbr title="required" class="required"></abbr></label>
                                        <input type="text" placeholder="Town / City" id="billing_city" name="city" class="input-text" required>
                                    </p>

                                    <p class="form-row form-row-wide address-field validate-required">
                                        <label for="billing_address_1">Address <abbr title="required" class="required"></abbr></label>
                                        <input type="text" placeholder="Street" id="billing_address_1" name="street" class="input-text " required>
                                    </p>

                                    <p class="form-row form-row-wide address-field">
                                        <input type="number" placeholder="House number" id="billing_address_2" name="house" class="input-text " required>
                                    </p>

                                    <p class="form-row form-row-wide address-field">
                                        <input type="number" placeholder="Flat number" id="billing_address_3" name="flat" class="input-text " required>
                                    </p>

                                    <p class="form-row form-row-first validate-required validate-email">
                                        <label for="billing_email">Email Address <abbr title="required" class="required"></abbr></label>
                                        <input type="text" id="billing_email" name="email" class="input-text " required>
                                    </p>

                                    <p class="form-row form-row-last validate-required validate-phone">
                                        <label for="billing_phone">Phone <abbr title="required" class="required"></abbr></label>
                                        <input type="text" id="billing_phone" name="telephone" class="input-text " required>
                                    </p>

                                    <p class="form-row form-row-last validate-required validate-phone">
                                        <label for="billing_iin">Iin <abbr title="required" class="required"></abbr></label>
                                        <input type="text" id="billing_iin" name="iin" class="input-text " required>
                                    </p>

                                    <p class="form-row form-row-last validate-required validate-phone">
                                        <div class="form-row place-order">
                                            <input type="submit"  value="Update data information">
                                        </div>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer-bottom-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="copyright">
                    <p>&copy; 2018 Vaymart. All Rights Reserved. <a href="http://www.vaymart.com" target="_blank">vaymart.com</a></p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="footer-card-icon">
                    <i class="fa fa-cc-discover"></i>
                    <i class="fa fa-cc-mastercard"></i>
                    <i class="fa fa-cc-paypal"></i>
                    <i class="fa fa-cc-visa"></i>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
</script>
<script src="/web/js/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="/web/js/view.js"></script>
<script src="/web/js/bootstrap.min.js"></script>
</body>
</html>

