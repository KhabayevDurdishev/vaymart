<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>Full Payment Page</title>

    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/web/css/bootstrap.min.css">
    <link rel="stylesheet" href="/web/css/owl.carousel.css">
    <link rel="stylesheet" href="/web/css/font-awesome.min.css">
    <link rel="stylesheet" href="/web/css/responsive.css">
    <link rel="stylesheet" href="/web/css/style.css">
</head>
<body>
<% int customerId = (int) request.getAttribute("customerId");
    String plan = (String) request.getAttribute("plan");
%>
<div class="header-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="user-menu">
                    <ul>
                        <li><a href="/webui/cart"><i class="fa fa-user"></i> My Cart</a></li>
                        <%  boolean flag = (boolean)request.getAttribute("session");
                        if(flag){ %>
                        <li><a href="/webui/account/"><i class="fa fa-user"></i> My Account</a></li>
                        <li><a href="/webui/logout"><i class="fa fa-user"></i> Logout</a></li>
                        <%}else{%>
                        <li><a href="/webui/login"><i class="fa fa-user"></i> Login</a></li>
                        <li><a href="/webui/signup"><i class="fa fa-user"></i> Sign Up</a></li>
                        <%}%>
                    </ul>
                </div>
            </div>

            <div class="col-md-4">
                <div class="header-right">
                    <ul class="list-unstyled list-inline">
                        <li class="dropdown dropdown-small">
                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">currency :</span><span class="value">USD </span><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">USD</a></li>
                            </ul>
                        </li>

                        <li class="dropdown dropdown-small">
                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">language :</span><span class="value">English </span><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">English</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="site-branding-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="logo">
                    <h1><a href="/webui/index">VAYMART</a></h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="product-big-title-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-bit-title text-center">
                    <h2>Enter your card information</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="width: 100%;height: 200px; margin-left: 45%;">
    <form action="/webui/payment" method="POST">
        <div id="customer_details" class="col2-set">
            <div class="col-1">
                <div class="woocommerce-billing-fields">
                    <h3 style="margin-top :10px;">Full Payment</h3>
                    <input type="hidden" name="customer" value="<%=customerId%>">
                    <input type="hidden" id="amount-id" name="amount" value="">
                    <input type="hidden"  name="plan" value="<%=plan%>">
                    <script
                        src="https://checkout.stripe.com/checkout.js"
                        id="my-script"
                        class="stripe-button"
                        data-key="pk_test_QUSj1FsxSfmVKGLQoHfMWQK6"
                        data-name="Stripe.com"
                        data-amount=""
                        data-description="Example charge"
                        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                        data-locale="auto">
                    </script>
                </div>
            </div>
        </div>
    </form>
</div>

<table class="shop_table">
    <thead>
        <tr>
            <th class="product-thumbnail">Photo</th>
            <th class="product-name">Product</th>
            <th class="product-total">Total</th>
        </tr>
    </thead>
    <tbody id="container"></tbody>
    <tfoot>
        <tr class="cart-subtotal">
            <th></th>
            <th>Cart Total</th>
            <td>
                <span class="amount"></span>
            </td>
        </tr>
    </tfoot>
</table>

<div class="footer-bottom-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="copyright">
                    <p>&copy; 2018 Vaymart. All Rights Reserved. <a href="http://www.vaymart.com" target="_blank">vaymart.com</a></p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="footer-card-icon">
                    <i class="fa fa-cc-discover"></i>
                    <i class="fa fa-cc-mastercard"></i>
                    <i class="fa fa-cc-paypal"></i>
                    <i class="fa fa-cc-visa"></i>
                </div>
            </div>
        </div>
    </div>
</div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous">
    </script>
    <script src="/web/js/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <script src="/web/js/bootstrap.min.js"></script>
    <script src="/web/js/view.js"></script>
</body>
</html>