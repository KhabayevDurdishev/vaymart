<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Installment-payment</title>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/web/css/bootstrap.min.css">
    <link rel="stylesheet" href="/web/css/owl.carousel.css">
    <link rel="stylesheet" href="/web/css/font-awesome.min.css">
    <link rel="stylesheet" href="/web/css/responsive.css">
    <link rel="stylesheet" href="/web/css/style.css">
</head>
<body>
<div class="header-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="user-menu">
                    <ul>
                        <li><a href="/webui/index"><i class="fa fa-user"></i> Main Menu</a></li>
                        <li><a href="/webui/account/"><i class="fa fa-user"></i> Account</a></li>
                        <li><a href="/webui/cart"><i class="fa fa-user"></i> My Cart</a></li>
                        <li><a href="/webui/logout"><i class="fa fa-user"></i> Logout</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-4">
                <div class="header-right">
                    <ul class="list-unstyled list-inline">
                        <li class="dropdown dropdown-small">
                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">currency :</span><span class="value">USD </span><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">USD</a></li>
                            </ul>
                        </li>

                        <li class="dropdown dropdown-small">
                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">language :</span><span class="value">English </span><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">English</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="site-branding-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="logo">
                    <h1><a href="/webui/index">VAYMART</a></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="product-big-title-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-bit-title text-center">
                    <h2>Installment-payment</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="single-product-area">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="product-content-right">
                    <div class="woocommerce">
                        <form action="/webui/account/installment-payment" class="checkout" method="post">
                            <div id="customer_details" class="col2-set">
                                <div class="col-1">
                                    <div class="woocommerce-billing-fields">
                                        <h3>Installment-payment</h3>
                                        <p id="billing_iin_field" class="form-row form-row-first validate-required">
                                            <label for="billing_iin">IIN<abbr title="required" class="required">*</abbr></label>
                                            <input type="text"  id="billing_iin" name="iin" class="input-text" required>
                                        </p>
                                        <p id="billing_invoice_field" class="form-row form-row-last validate-required">
                                            <label for="billing_invoice">Invoice Code<abbr title="required" class="required">*</abbr></label>
                                            <input type="password" id="billing_invoice" name="invoice" class="input-text" required>
                                        </p>
                                        <script
                                            src="https://checkout.stripe.com/checkout.js"
                                            class="stripe-button"
                                            id="my-script"
                                            data-key="pk_test_QUSj1FsxSfmVKGLQoHfMWQK6"
                                            data-name="Stripe.com"
                                            data-amount=""
                                            data-description="Example charge"
                                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                            data-locale="auto">
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="woocommerce">
                    <div class="cart-collaterals">
                        <div class="cart_totals ">
                            <h2>Payment</h2>
                            <table cellspacing="0">
                                <tbody>
                                <tr>
                                    <th>Installment date</th>
                                    <td>22.4.2018</td>
                                </tr>
                                <tr class="shipping">
                                    <th>Shipping and Handling</th>
                                    <td>Free Shipping</td>
                                </tr>

                                <tr class="order-total">
                                    <th>Order Total</th>
                                    <td><strong><span>1158$</span></strong> </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="footer-bottom-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="copyright">
                    <p>&copy; 2018 Vaymart. All Rights Reserved. <a href="http://www.vaymart.com" target="_blank">vaymart.com</a></p>
                </div>
            </div>

            <div class="col-md-4">
                <div class="footer-card-icon">
                    <i class="fa fa-cc-discover"></i>
                    <i class="fa fa-cc-mastercard"></i>
                    <i class="fa fa-cc-paypal"></i>
                    <i class="fa fa-cc-visa"></i>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
</script>
<script src="/web/js/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="/web/js/bootstrap.min.js"></script>

</body>
</html>
