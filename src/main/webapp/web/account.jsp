<%@ page import="com.team.dto.eshop.OrderDTO" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.team.dto.manager.Product" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">

    <title>Account Page</title>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/web/css/bootstrap.min.css">
    <link rel="stylesheet" href="/web/css/owl.carousel.css">
    <link rel="stylesheet" href="/web/css/font-awesome.min.css">
    <link rel="stylesheet" href="/web/css/responsive.css">
    <link rel="stylesheet" href="/web/css/style.css">
  </head>
  <body>
    <div class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="user-menu">
                        <ul>
                            <li><a href="/webui/index"><i class="fa fa-user"></i> Main Menu</a></li>
                            <li><a href="/webui/cart"><i class="fa fa-user"></i> My Cart</a></li>
                            <li><a href="/webui/logout"><i class="fa fa-user"></i> Logout</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="header-right">
                        <ul class="list-unstyled list-inline">
                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">currency :</span><span class="value">USD </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">USD</a></li>
                                </ul>
                            </li>

                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">language :</span><span class="value">English </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">English</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="logo">
                        <h1><a href="/webui/index">VAYMART</a></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>My Account</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div style="width: 100%; height: 350px">
                        <p style="margin-top: 20px;" class="form-row place-order">
                            <a href="/webui/account/user-information">
                                <input type="submit" style="width: 200px;" value="Data user">
                            </a>
                        </p>
                        <p style="margin-top: 20px;" class="form-row place-order">
                            <a href="/webui/account/installment-payment">
                                <input type="submit" style="width: 200px;"  value="Installment payment">
                            </a>
                        </p>
                        <p style="margin-top: 20px;" class="form-row place-order">
                            <a href="/webui/account/history">
                                <input type="submit" style="width: 200px;"  value="Orders History">
                            </a>
                        </p>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="product-content-right">
                        <div class="woocommerce">
                            <form action="/webui/account/cancel-payment" class="checkout" method="post">
                                <div id="customer_details" class="col2-set">
                                    <div class="col-1">
                                        <div class="woocommerce-billing-fields">
                                            <h3>Cancel payment</h3>
                                            <p style="color: red">
                                                You can cancel the payment after returning the goods to the warehouse!
                                            </p>
                                            <p id="billing_iin_field" class="form-row form-row-first validate-required">
                                                <label for="billing_iin">IIN<abbr title="required" class="required">*</abbr></label>
                                                <input type="text"  id="billing_iin" name="iin" class="input-text" required>
                                            </p>
                                            <p style="color: red">
                                                You can cancel the purchase by order or invoice code. Sent to you by email!
                                            </p>
                                            <p>Order Code :
                                                <input type="radio" id="radio-order" checked="checked" onclick="functionClick(this)">
                                            </p>
                                            <p>Invoice Code :
                                                <input type="radio" id="radio-invoice" onclick="functionClick(this)">
                                            </p>
                                            <p id="billing_order_field" class="form-row form-row-last validate-required">
                                                <label for="billing_order">Order Code<abbr title="Order" class="required">*</abbr></label>
                                                <input type="password" id="billing_order" name="order" class="input-text" required>
                                            </p>

                                            <p class="form-row form-row-last place-order validate-required">
                                                <input type="submit" value="Send cancel payment">
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="product-content-right">
                        <div class="woocommerce">
                            <%
                                OrderDTO orderDTO = (OrderDTO) request.getAttribute("order");
                                ArrayList<Product> list = (ArrayList<Product>) request.getAttribute("list");
                            %>
                            <form method="get" action="/webui/cart">
                                <table cellspacing="0" class="shop_table cart">
                                    <h2 style="color: #2a6496">LAST ORDER</h2>
                                    <thead>
                                        <tr>
                                            <th class="product-thumbnail">Order Code : <%=orderDTO.getOrderCode()%></th>
                                            <th class="product-name">Product</th>
                                            <th class="product-name">Color</th>
                                            <th class="product-price">Price</th>
                                            <th class="product-quantity">Quantity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <%
                                        for (Product p : list) {
                                    %>
                                    <tr class="cart_item">
                                        <td class="product-thumbnail">
                                            <a href="/webui/single-product"><img width="150" height="150" class="shop_thumbnail" src="<%=p.getLink()%>"></a><h2 style="float:right;margin-right: 35%;padding-top: 20px"></h2>
                                        </td>
                                        <td class="product-name">
                                            <%=p.getBrand().toUpperCase()%> <%=p.getModel().toUpperCase()%>
                                        </td>
                                        <td class="product-name">
                                            <span class="inner-amount"><%=p.getColor().toUpperCase()%></span>
                                        </td>
                                        <td class="product-price">
                                            <span class="inner-amount">$<%=p.getPrice()%>.00</span>
                                        </td>
                                        <td class="product-quantity">
                                            <span class="inner-amount">X 1</span>
                                        </td>
                                    </tr>
                                    <%}%>

                                    </tbody>
                                </table>
                            </form>
                            <div class="cart-collaterals">
                                <div class="cart_totals ">
                                    <h2>Order totals</h2>
                                    <table cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <th>Order date</th>
                                                <td><%=orderDTO.getDate()==null ? "NO DATE" : orderDTO.getDate()%></td>
                                            </tr>
                                            <tr class="shipping">
                                                <th>Shipping and Handling</th>
                                                <td>Free Shipping</td>
                                            </tr>

                                            <tr class="order-total">
                                                <th>Order Total</th>
                                                <td><strong><span><%=orderDTO.getTotalAmount()%>.00$</span></strong> </td>
                                            </tr>

                                            <tr>
                                                <th>General Status</th>
                                                <td><strong><span><%=orderDTO.getStatus() == null? "NO STATUS" : orderDTO.getStatus()%></span></strong> </td>
                                            </tr>

                                            <tr>
                                                <th>Payment Status</th>
                                                <td><strong><span><%=orderDTO.getPaymentStatus()==null ? "NO STATUS" : orderDTO.getPaymentStatus()%></span></strong> </td>
                                            </tr>

                                            <tr>
                                                <th>Warehouse Status</th>
                                                <td><strong><span><%=orderDTO.getWareHouseStatus()==null?"NO STATUS":orderDTO.getWareHouseStatus()%></span></strong> </td>
                                            </tr>

                                            <tr>
                                                <th>Delivery Status</th>
                                                <td><strong><span><%=orderDTO.getDeliveryStatus()==null?"NO STATUS":orderDTO.getDeliveryStatus()%></span></strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="copyright">
                        <p>&copy; 2018 Vaymart. All Rights Reserved. <a href="http://www.vaymart.com" target="_blank">vaymart.com</a></p>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="footer-card-icon">
                        <i class="fa fa-cc-discover"></i>
                        <i class="fa fa-cc-mastercard"></i>
                        <i class="fa fa-cc-paypal"></i>
                        <i class="fa fa-cc-visa"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous">
    </script>
    <script src="/web/js/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <script src="/web/js/bootstrap.min.js"></script>
    <script>
      function functionClick(elem) {
          var container = $("#billing_order_field").empty();
          if(elem.id==='radio-invoice'){
              $("#radio-order").prop('checked',false);
              $("#radio-invoice").prop('checked',true);
              container.append('<label for="billing_invoice">Invoice Code<abbr title="Invoice" class="required">*</abbr></label>' +
                    '<input type="password" id="billing_invoice" name="invoice" class="input-text" required>'
                  );
          }
          else{
              $("#radio-invoice").prop('checked',false);
              $("#radio-order").prop('checked',true);
              container.append('<label for="billing_order">Order Code<abbr title="Order" class="required">*</abbr></label>' +
                    '<input type="password" id="billing_order" name="order" class="input-text" required>'
              );
          }
      }
    </script>

  </body>
</html>