<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>Add Products</title>

    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/web/css/bootstrap.min.css">
    <link rel="stylesheet" href="/web/css/owl.carousel.css">
    <link rel="stylesheet" href="/web/css/font-awesome.min.css">
    <link rel="stylesheet" href="/web/css/responsive.css">
    <link rel="stylesheet" href="/web/css/style.css">


</head>
<body>
<div class="header-area">
    <div class="container">
        <div class="row">
                <div class="col-md-4">
                    <div class="header-right">
                        <ul class="list-unstyled list-inline">
                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">currency :</span><span class="value">USD </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">USD</a></li>
                                </ul>
                            </li>

                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">language :</span><span class="value">English </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">English</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mainmenu-area">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Add Products</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Search Products</h2>
                        <form action="">
                            <input type="text" placeholder="Search products...">
                            <input type="submit" value="Search">
                        </form>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="product-content-right">
                        <div class="woocommerce">
                            <form action="/manager/add" method="POST">
                                <div id="customer_details" class="col2-set">
                                    <div class="col-1">
                                        <p class="woocommerce-billing-fields">
                                            <h3>Product Details</h3>
                                            <p class="form-row form-row-first validate-required">
                                                <label for="billing_brand">Brand <abbr title="required" class="required">*</abbr></label>
                                                <input type="text" id="billing_brand" name="brand" class="input-text" required>
                                            </p>

                                            <p class="form-row form-row-last validate-required">
                                                <label for="billing_model">Model <abbr title="required" class="required">*</abbr></label>
                                                <input type="text" id="billing_model" name="model" class="input-text" required>
                                            </p>

                                            <p class="form-row form-row-last validate-required">
                                                <label for="billing_photo">Photo <abbr title="required" class="required">*</abbr></label>
                                                <input type="file" accept="image/*" id="billing_photo" name="photo" class="input-text" required>
                                            </p>

                                            <p class="form-row form-row-last validate-required">
                                                <label for="billing_color">Color <abbr title="required" class="required">*</abbr></label>
                                                <input type="text" id="billing_color" name="color" class="input-text" required>
                                            </p>

                                            <p class="form-row form-row-last validate-required">
                                                <label for="billing_memory_size">Memory Size <abbr title="required" class="required">*</abbr></label>
                                                <input type="text" id="billing_memory_size" name="memory" class="input-text" required>
                                            </p>

                                            <p class="form-row form-row-first validate-required validate-email">
                                                <label for="billing_price">Price <abbr title="required" class="required">*</abbr></label>
                                                <input type="text" id="billing_price" name="price" class="input-text " required>
                                            </p>

                                            <div class="form-row place-order">
                                                <input type="submit" data-value="Add Product" value="Place order" id="place_order" name="woocommerce_checkout_place_order" class="button alt">
                                            </div>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom-area">
        <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="copyright">
                            <p>&copy; 2018 Vaymart. All Rights Reserved. <a href="http://www.vaymart.com" target="_blank">vaymart.com</a></p>
                        </div>
                </div>

                <div class="col-md-4">
                    <div class="footer-card-icon">
                        <i class="fa fa-cc-discover"></i>
                        <i class="fa fa-cc-mastercard"></i>
                        <i class="fa fa-cc-paypal"></i>
                        <i class="fa fa-cc-visa"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="/web/js/jquery.min.js"></script>
<script src="/web/js/bootstrap.min.js"></script>
</body>
</html>
