var container = $('#container');
var arrProduct;
var count = 1;
var totalPriceClass = $(".amount");
var countProduct = $('#product-count-id');

$.ajax({
    url: "/api/products/get",
    type: "GET",
    dataType: "json",
    success: function (data) {
        arrProduct = data;
        fetchDataAndDisplay(count);
    }
});

function getCookie() {
    var cookie = $.cookie("products");
    if(cookie !== undefined){
        return JSON.parse(cookie);
    }else{
        return 0;
    }
}

function fetchDataAndDisplay() {
    var jsonCookie = getCookie();
    countProduct.append(jsonCookie !== 0 ? jsonCookie.length : 0);
    var totalPrice = 0;
    if(jsonCookie !== 0){
    $.each(jsonCookie, function (index, cookie) {
        $.each(arrProduct, function (index, item) {
            if (cookie.vendorCode === index) {
                container.append(
                    '<tr class="cart_item">' +
                    '<td class="product-remove">' +
                    '<a title="Remove this item" class="remove" href="/webui/cart?remove=' + item.vendorCode + '">Remove</a>' +
                    '</td>' +
                    '<td class="product-thumbnail">' +
                    '<a href="/webui/single-product"><img width="150" height="150" class="shop_thumbnail" src="' + item.link + '"></a>' +
                    '</td>' +
                    '<td class="product-name">' + item.brand.toUpperCase() + ' ' + item.model.toUpperCase() + '</td>' +
                    '<td class="product-price">' +
                    '<span class="inner-amount">$' + item.price + '.00</span>' +
                    '</td>' +
                    '<td class="product-quantity" colspan="6">' +
                    '<div class="quantity buttons_added">' +
                    '<input type="number" name="quantity" class="input-text qty text" value="1" min="1" max="10" step="1">' +
                    '</div>' +
                    '</td>' +
                    '</tr>'
                );
                totalPrice += item.price;
            }
        });
    });
    }
    if(totalPrice !== 0 ){
        container.append(
            '<tr>' +
            '<td class="actions" colspan="6">' +
            '<input type="submit" value="Checkout" name="proceed" class="checkout-button button alt wc-forward">' +
            '</td>' +
            '</tr>');
    }
    for (var i = 0; i < totalPriceClass.length; i++) {
        totalPriceClass[i].append(totalPrice !== 0 ? '$' + totalPrice + '.00' : '$' + 0 + '.00');
    }
}
