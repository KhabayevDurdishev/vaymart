var container = $('#my-container');
var arrProduct;
var buttonNumber = $('#result-val');
//
// $(".btn").click(function(){
//     count = $(this).val();
//     fetchDataAndDisplay();
// });

$.ajax({
    url: "/api/products/size",
    type: "GET",
    dataType: "json",
    success: function(data) {
        arrProduct = data;
        buttonNumberMethod();
        fetchDataAndDisplay(1);
    }
});

function buttonNumberMethod() {
    var size = Object.keys(arrProduct).length + 1;
    for(var i = 1; i<size; i++){
        buttonNumber.append('<li>'+'<button onclick="fetchDataAndDisplay('+i+')" >'+i+'</button>'+' </li> ');
    }
}

function fetchDataAndDisplay(elem) {
    $.each(arrProduct, function (index, item) {
        if (elem == index) {
            container.empty();
            $.each(item, function (indexInner, value) {
                console.log(value.productId);
                container.append(
                    '<div class="col-md-3 col-sm-6">'+
                    '<div style="margin-bottom: 30px;" class="single-product">'+
                    '<div class="product-f-image">'+
                    '<img src="' + value.link + '" alt="">'+
                    '<div class="product-hover">'+
                    '<a href="/webui/cart?mobile='+ value.vendorCode + '&productId=' +value.productId+ '" class="add-to-cart-link">Add to cart</a>' +
                    '<a href="/webui/single-product" class="view-details-link">See details</a>' +
                    '</div>' +
                    '</div>' +
                    '<h2><a style="color:darkblue" href="">' + value.brand.toUpperCase() + ' ' + value.model.toUpperCase() + '<br>COLOR: ' + value.color.toUpperCase() + '<br>PRICE: <ins>' + value.price + '.00 $</ins></a></h2>'+
                    '</div>'+
                    '</div>');
            });
        }
    });
}