var arrProduct;

$.ajax({
    url: "/api/products/get",
    type: "GET",
    dataType: "json",
    success: function (data) {
        arrProduct = data;
        fetchDataAndDisplay();
    }
});

function fetchDataAndDisplay() {
    var out = $(".latest-product").append('<div id="prodcaro" class="product-carousel">');
    var container = $('#prodcaro');
    $.each(arrProduct, function (index, item) {
        container.append(
            '<div class="single-product">'+
                '<div style="padding-top: 20px; height: 270px" class="product-f-image">' +
                '<img src="'+item.link+'" alt="">'+
                    '<div class="product-hover">'+
                        '<a href="/webui/cart?mobile='+item.vendorCode+'" class="add-to-cart-link"><i class="fa fa-shopping-cart"></i>Add to cart</a>' +
                        '<a href="/webui/single-product" class="view-details-link"><i class="fa fa-link"></i> See details</a>' +
                    '</div>' +
                '</div>'+
                '<h2><a style="color:darkblue" href="">'+item.brand.toUpperCase()+' '+item.model.toUpperCase()+'<br>COLOR: '+item.color.toUpperCase()+'<br>PRICE: <ins>'+item.price+'.00 $</ins></a></h2>'+
            '</div>');

    });
    out.append('</div>');
}