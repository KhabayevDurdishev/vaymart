<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="UTF-8">
    <title>Login Page</title>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="/web/css/bootstrap.min.css">
      <link rel="stylesheet" href="/web/css/owl.carousel.css">
      <link rel="stylesheet" href="/web/css/font-awesome.min.css">
      <link rel="stylesheet" href="/web/css/responsive.css">
      <link rel="stylesheet" href="/web/css/style.css">
  </head>
  <body>
    <div class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="user-menu">
                        <ul>
                            <li><a href="/webui/login"><i class="fa fa-user"></i> Login</a></li>
                            <li><a href="/webui/signup"><i class="fa fa-user"></i>Sign Up</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="header-right">
                        <ul class="list-unstyled list-inline">
                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">currency :</span><span class="value">USD </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">USD</a></li>
                                </ul>
                            </li>

                            <li class="dropdown dropdown-small">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">language :</span><span class="value">English </span><b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">English</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="logo">
                        <h1><a href="/webui">VAYMART</a></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Login</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <div class="product-content-right">
                        <div class="woocommerce">
                            <form action="/webui/login" class="checkout" method="post" name="checkout">
                                <div id="customer_details" class="col2-set">
                                    <div class="col-1">
                                        <div class="woocommerce-billing-fields">
                                            <h3>LOGIN</h3>
                                            <p id="billing_email_field" class="form-row form-row-first validate-required">
                                                <label  for="billing_email">Email<abbr title="required" class="required">*</abbr></label>
                                                <input type="text"  id="billing_email" name="email" class="input-text" required>
                                            </p>
                                            <p id="billing_password_field" class="form-row form-row-last validate-required">
                                                <label for="billing_password">Password<abbr title="required" class="required">*</abbr></label>
                                                <input type="password" id="billing_password" name="password" class="input-text" required>
                                            </p>
                                            <p id="forgot_password" class="form-row form-row-last validate-required">
                                                <a href="#">Forgot password?</a>
                                            </p>
                                            <p class="form-row form-row-last place-order validate-required">
                                                <input type="submit" value="Login">
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>                       
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="copyright">
                        <p>&copy; 2018 Vaymart. All Rights Reserved. <a href="http://www.vaymart.com" target="_blank">vaymart.com</a></p>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="footer-card-icon">
                        <i class="fa fa-cc-discover"></i>
                        <i class="fa fa-cc-mastercard"></i>
                        <i class="fa fa-cc-paypal"></i>
                        <i class="fa fa-cc-visa"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="/web/js/jquery.min.js"></script>
    <script src="/web/js/bootstrap.min.js"></script>
  </body>
</html>