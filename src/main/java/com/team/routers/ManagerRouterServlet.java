package com.team.routers;

import com.team.dto.manager.Manager;
import com.team.dto.manager.MobilePhone;
import com.team.dto.manager.ProductDTO;
import com.team.dto.manager.ShopWindow;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.services.ManagerService;
import com.team.services.ProductService;
import com.team.utils.Encrypt;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/manager/*")
public class ManagerRouterServlet extends HttpServlet {

    private ManagerService managerService = new ManagerService();
    private ProductService productService = new ProductService();

    private void route(HttpServletRequest req, HttpServletResponse resp){

        String uri = req.getRequestURI();
        try {
            switch (uri) {
                case "/manager/main": {
                    req.getRequestDispatcher("/web/main-manager.jsp").forward(req, resp);
                    break;
                }
                case "/manager/login": {
                    String email = req.getParameter("email");
                    String password = req.getParameter("password");
                    if(email!=null && password!=null){
                        boolean isBoolean = managerService.checkManager(new Manager(email, Encrypt.getMD5(password)));
                        if(isBoolean){
                            req.getRequestDispatcher("/web/add-manager.jsp").forward(req, resp);
                            break;
                        }
                    }
                    req.getRequestDispatcher("/web/login-manager.jsp").forward(req, resp);
                    break;
                }
                case "/manager/signup": {
                    //Нужно в эту систему внедрить superuser который будет делать регис.
                    String email = req.getParameter("email");
                    String password = req.getParameter("password");
                    req.getRequestDispatcher("/web/signup-manager.jsp").forward(req, resp);
                    break;
                }
                case "/manager/add": {
                    String brand = req.getParameter("brand");
                    String model = req.getParameter("model");
                    String color = req.getParameter("color");
                    String memory = req.getParameter("memory");
                    String price = req.getParameter("price");
                    String photo = req.getParameter("photo");
                    if(brand != null){
                        //Заменить на другую систему генер. вердер кода!
                        String vendorCode = Encrypt.getMD5(brand.toUpperCase() + model.toUpperCase() + color.toUpperCase() + memory);
                        MobilePhone mob = new MobilePhone(brand,model,color,Integer.parseInt(memory));
                        String link = "/web/img/" + photo;
                        ShopWindow shop = new ShopWindow(vendorCode,Integer.parseInt(price),1);
                        ProductDTO product = new ProductDTO(mob,link,shop);
                        productService.addProductInDataBase(product);
                    }
                    req.getRequestDispatcher("/web/add-manager.jsp").forward(req, resp);
                    break;
                }
            }
        } catch (ServletException | IOException e) {
            try {
                resp.getWriter().write(e.getMessage());
            } catch (IOException exc) {
                exc.printStackTrace();
            }
        } catch (VaymartDataBaseException e) {
            try {
                switch (e.getVaymartDataBaseError()) {
                    case INTERNAL_ERROR:
                        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        resp.getWriter().write("{\"msg\":\"" + e.getMessageError() + "\"}");
                        break;
                    case STORAGE_ERROR:
                        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        resp.getWriter().write("{\"msg\":\"" + e.getMessageError() + "\"}");
                        break;
                    default:
                        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        resp.getWriter().write("{\"msg\":\"Unimplemented error type\"}");
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doHead(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doTrace(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }
}
