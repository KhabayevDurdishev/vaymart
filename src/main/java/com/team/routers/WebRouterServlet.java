package com.team.routers;

import com.team.controllers.OrderController;
import com.team.dto.eshop.*;
import com.team.dto.manager.Product;
import com.team.dto.payment.Customer;
import com.team.dto.payment.PaymentOrder;
import com.team.dto.payment.Plan;
import com.team.dto.payment.StripeCharge;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.exсeptions.VaymartPaymentException;
import com.team.exсeptions.VaymartStripeException;
import com.team.services.*;
import com.team.utils.Encrypt;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/webui/*")
public class WebRouterServlet extends HttpServlet {

    private CustomerService customerService = new CustomerService();
    private CartService cartService = new CartService();
    private OrderService orderService = new OrderService();
    private PaymentService paymentService = new PaymentService();
    private ProductService productService = new ProductService();
    private StripeApiService stripeApiService = new StripeApiService();
    private UserService userService = new UserService();

    private OrderController orderController = new OrderController();

    private void route(HttpServletRequest req, HttpServletResponse resp){

        String uri = req.getRequestURI();
        HttpSession session = req.getSession(true);

        try {
            switch (uri) {

                case "/webui/login": {
                    String email = req.getParameter("email");
                    String password = req.getParameter("password");
                    if(email == null || password == null){
                        req.getRequestDispatcher("/web/login.jsp").forward(req,resp);
                    }
                    else{
                        boolean tmpBoolean = userService.checkMail(email.toLowerCase(), Encrypt.getMD5(password));
                        if(tmpBoolean){
                            session.setAttribute("email",email.toLowerCase());
                            session.setAttribute("password", Encrypt.getMD5(password));
                            resp.sendRedirect("/webui/index");
                        }else{
                            req.getRequestDispatcher("/web/login.jsp").forward(req,resp);
                        }
                    }
                    break;
                }

                case "/webui": {
                    req.getRequestDispatcher("/web/main.jsp").forward(req,resp);
                    break;
                }

                case "/webui/signup": {
                    String email = req.getParameter("email");
                    String password = req.getParameter("password");
                    if(email!=null && password!=null && email.length() > 0 && password.length() > 0) {
                        boolean tmpBoolean = userService.mailCheckForEmployment(email);
                        if (tmpBoolean) {
                            resp.sendRedirect("/webui/activate");
                            userService.addUsersInDataBase(new User(email.toLowerCase(), Encrypt.getMD5(password), false));
                            return;
                        } else {
                            req.setAttribute("reserveMail",true);
                            req.getRequestDispatcher("/web/signup.jsp").forward(req,resp);
                            return;
                        }
                    }
                    req.getRequestDispatcher("/web/signup.jsp").forward(req,resp);
                    break;
                }

                case "/webui/index": {
                    req.setAttribute("allProducts", productService.getAllProducts());
                    req.getRequestDispatcher("/web/index.jsp").forward(req,resp);
                    break;
                }

                case "/webui/shop": {
                    req.getRequestDispatcher("/web/shop.jsp").forward(req,resp);
                    break;
                }

                case "/webui/checkout": {
                    String emailUser = (String) session.getAttribute("email");
                    String password = (String) session.getAttribute("password");

                    //Доработать в Js что бы из формы не прилетали пустые поля!
                    CustomerCharge charge = CustomerCharge.builder()
                        .name(req.getParameter("name")).surname(req.getParameter("surname"))
                        .city(req.getParameter("city")).street(req.getParameter("street"))
                        .houseNum(req.getParameter("house")).flatNum(req.getParameter("flat"))
                        .email(req.getParameter("email")).tel(req.getParameter("telephone"))
                        .iin(req.getParameter("iin")).payment(req.getParameter("plan")).build();

//                    boolean flag = customerService.checkForExistenceCustomers(email, iin);
//                    System.out.println(flag);
                    if (charge.getPayment()!=null) {
                        Person person = new Person(charge.getName(),charge.getSurname());
                        IdentificationData idData = new IdentificationData(charge.getIin(),charge.getEmail(),charge.getTel());
                        Location location = new Location(charge.getCity(),charge.getStreet(),charge.getHouseNum(),charge.getFlatNum());
                        CustomerDTO customer = new CustomerDTO(person,idData,location);
                        int customerId = customerService.addCustomerInDataBase(customer);

                        if(emailUser!=null && password!=null) {
                            userService.addCustomerInUserTable(customerId, emailUser, password);
                        }
                        // Добавить работу с сервисом Warehouse
                        // Зарезервировать товар и снять количество со склада

                        req.setAttribute("customerId", customerId);
                        req.setAttribute("plan", charge.getPayment());
                    }
                    if(charge.getPayment() != null && charge.getPayment().equals("instant")){
                        req.getRequestDispatcher("/web/full.jsp").forward(req,resp);
                    }else if(charge.getPayment() != null && charge.getPayment().equals("installment")){
                        req.getRequestDispatcher("/web/installment.jsp").forward(req,resp);
                    }else{
                        req.getRequestDispatcher("/web/checkout.jsp").forward(req, resp);
                    }
                    break;
                }

                case "/webui/account/": {
                    String email = (String)session.getAttribute("email");
                    if(email!=null){
                        int customerId = userService.getLastCustomerId(email);
                        if(customerId != 0) {
                            OrderDTO orderDTO = orderService.getOrderByCustomerId(customerId);
                            ArrayList<Product> list= orderService.getProductsIdByOrderCode(orderDTO.getOrderCode());
                            req.setAttribute("list",list);
                            req.setAttribute("order", orderDTO);
                        }
                    }
                    req.getRequestDispatcher("/web/account.jsp").forward(req,resp);
                    break;
                }

                case "/webui/activate": {
                    String email = req.getParameter("email");
                    String password = req.getParameter("md");
                    if(email != null && password != null) {
                        userService.updateActivation(new User(email, password, true));
                        boolean tmpBoolean = userService.checkMail(email.toLowerCase(), password);
                        if(tmpBoolean){
                            session.setAttribute("email",email.toLowerCase());
                            session.setAttribute("password", password);
                            resp.sendRedirect("/webui/index");
                            return;
                        }
                    }
                    req.getRequestDispatcher("/web/activate.jsp").forward(req,resp);
                    break;
                }

                case "/webui/cart": {
                    String vendorCode = req.getParameter("mobile");
                    String productId = req.getParameter("productId");
                    String removeCode = req.getParameter("remove");
                    String[] quantityMobilePhone = req.getParameterValues("quantity");
                    Cookie foundCookie = cartService.findProducts(req.getCookies());

                    if (vendorCode != null) {
                        Cookie cookie = cartService.createCookieFromJson(foundCookie, vendorCode, productId);
                        resp.addCookie(cookie);
                        resp.sendRedirect("/webui/shop");
                        return;
                    } else if (foundCookie != null && removeCode != null) {
                        Cookie cookie = cartService.removeCookie(foundCookie, removeCode);
                        resp.addCookie(cookie);
                        resp.sendRedirect("/webui/cart");
                        return;
                    } else if (quantityMobilePhone != null) {
                        //cartService.setQuantity(quantityMobilePhone, foundCookie);
                        resp.sendRedirect("/webui/checkout");
                        return;
                    }
                    req.getRequestDispatcher("/web/cart.jsp").forward(req, resp);
                    break;
                }

                case "/webui/payment": {
                    StripeCharge charge = StripeCharge.builder()
                        .amount(req.getParameter("amount"))
                        .customerId(req.getParameter("customer"))
                        .month(req.getParameter("month"))
                        .plan(req.getParameter("plan"))
                        .token(req.getParameter("stripeToken"))
                        .build();

                    if(charge.getCustomerId() != null && charge.getToken() != null) {
                        Customer customer = customerService.getCustomerById(charge.getCustomerId());
                        String stripeKey = stripeApiService.getCustomerIdFromStripe(customer.getEmail(),charge.getToken());
                        customer.setStripeKey(stripeKey);
                        customerService.addCustomerStripeId(customer.getCustomerKey(), stripeKey);

                        //  Сделать что бы дата прилетала из формы!
                        int orderCode = orderService.addOrderInDataBase(charge.getCustomerId(),charge.getAmount());
                        Cookie productsJson = cartService.findProducts(req.getCookies());
                        orderController.fromJsonToProductAndAddProductInOrder(productsJson,orderCode);
                        Cookie cookie = cartService.removeAll(req.getCookies());
                        resp.addCookie(cookie);

                        Plan plan = new Plan(charge.getPlan(),charge.getMonth());
                        int amount = Integer.parseInt(charge.getAmount())*100;
                        PaymentOrder paymentOrder = new PaymentOrder(orderCode,amount,customer,plan);

                        paymentService.sendOrderInPaymentSystem(paymentOrder,charge.getToken());
                    }

                    if(charge.getToken() == null){
                        switch (charge.getPlan()){
                            case "instant" :{
                                req.getRequestDispatcher("/web/full.jsp").forward(req, resp);
                                break;
                            }
                            case "installment" :{
                                req.getRequestDispatcher("/web/installment.jsp").forward(req, resp);
                                break;
                            }
                        }
                    }else{
                        req.getRequestDispatcher("/web/checkout.jsp").forward(req, resp);
                    }
                    break;
                }

                case "/webui/single-product": {
                    req.getRequestDispatcher("/web/single-product.jsp").forward(req,resp);
                    break;
                }

                case "/webui/logout": {
                    session.removeAttribute("email");
                    session.removeAttribute("password");
                    resp.sendRedirect("/webui");
                    break;
                }

                case "/webui/account/installment-payment": {
                    String iin = req.getParameter("iin");
                    String invoice = req.getParameter("invoice");
                    String token = req.getParameter("stripeToken");
                    if(iin != null) {
                        String customerKey = customerService.getCustomerKeyByIin(iin);
                        if (customerKey != null) {
                            stripeApiService.addCardInCustomer(customerKey, token);


                        }
                    }
                    req.getRequestDispatcher("/web/installment-payment.jsp").forward(req,resp);
                    break;
                }

                case "/webui/answer": {
                    req.getRequestDispatcher("/web/answer-payment.jsp").forward(req,resp);
                    break;
                }

                case "/webui/account/user-information": {
                    req.getRequestDispatcher("/web/user-data.jsp").forward(req,resp);
                    break;
                }

                case "/webui/account/cancel-payment": {
                    String iin = req.getParameter("iin");
                    String order = req.getParameter("order");
                    String invoice = req.getParameter("invoice");

                    if(iin!=null) {
                        boolean checkingIIN = customerService.checkingIinInDataBase(iin);
                        req.setAttribute("iin", checkingIIN);
                    }

                    req.getRequestDispatcher("/web/account.jsp").forward(req,resp);
                    break;
                }

                case "/webui/account/history": {
                    req.getRequestDispatcher("/web/order-history.jsp").forward(req,resp);
                    break;
                }

            }

        }catch (VaymartPaymentException e) {
            e.printStackTrace();
            switch (e.getVaymartPaymentError()) {
                case SYSTEM_IS_NOT_RESPONDING:
                    resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    try {
                        resp.getWriter().write("{\"msg\":\"" + e.getMessage() + "\"}");
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    break;
                default:
                    resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    try {
                        resp.getWriter().write("{\"msg\":\"Unknown error\"}");
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
            }
        } catch (VaymartDataBaseException e) {
            try {
                switch (e.getVaymartDataBaseError()) {
                    case INTERNAL_ERROR:
                        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        resp.getWriter().write("{\"msg\":\"" + e.getMessageError() + "\"}");
                        break;
                    case STORAGE_ERROR:
                        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        resp.getWriter().write("{\"msg\":\"" + e.getMessageError() + "\"}");
                        break;
                    default:
                        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                        resp.getWriter().write("{\"msg\":\"Unknown error\"}");
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        } catch (VaymartStripeException e) {
            try {
                switch (e.getStripeError()) {
                    case EMAIL_ERROR:
                        resp.setStatus(HttpServletResponse.SC_PAYMENT_REQUIRED);
                        resp.getWriter().write("{\"msg\":\"" + e.getMessage() + "\"}");
                        break;
                    case CARD_NUMBER_ERROR:
                        resp.setStatus(HttpServletResponse.SC_PAYMENT_REQUIRED);
                        resp.getWriter().write("{\"msg\":\"" + e.getMessage() + "\"}");
                        break;
                    case CVC_ERROR:
                        resp.setStatus(HttpServletResponse.SC_PAYMENT_REQUIRED);
                        resp.getWriter().write("{\"msg\":\"" + e.getMessage() + "\"}");
                        break;
                    default:
                        resp.setStatus(HttpServletResponse.SC_PAYMENT_REQUIRED);
                        resp.getWriter().write("{\"msg\":\"Unimplemented error type\"}");
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        } catch (IOException | ServletException e) {
            try {
                resp.getWriter().write(e.getMessage());
            } catch (IOException exc) {
                exc.printStackTrace();
            }
        }
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doHead(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }

    @Override
    protected void doTrace(HttpServletRequest req, HttpServletResponse resp){
        route(req, resp);
    }
}
