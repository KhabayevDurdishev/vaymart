package com.team.routers;

import com.team.controllers.PaymentController;
import com.team.controllers.ProductController;
import com.team.exсeptions.VaymartDataBaseException;

import java.io.BufferedReader;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/api/*")
public class ApiRouterServlet extends HttpServlet {

  private ProductController productController = new ProductController();
  private PaymentController paymentController = new PaymentController();

  private void route(HttpServletRequest req, HttpServletResponse resp){
    String uri = req.getRequestURI();
    try {
        BufferedReader reader = req.getReader();

        StringBuilder stringBuilder = new StringBuilder();
        String str;
        while ((str = reader.readLine()) != null) {
            stringBuilder.append(str);
        }

        String respJson = "";
        try {
            switch (uri) {
                case "/api/products/get": {
                    respJson = productController.getAllProductsFromMapToJson();
                    break;
                }
                case "/api/products/set": {
                    productController.setProductFromJsonInDataBase(stringBuilder.toString());
                    break;
                }
                case "/api/products/size": {
                    respJson = productController.getProductsBySizeFromMapToJson();
                    break;
                }
                case "/api/customer/id/mail": {
                    // Получаю StripeCustomerId возвращаю email customer;
                    respJson = paymentController.getEmailByCustomerId(stringBuilder.toString());
                    break;
                }
                case "/api/products/test": {
                    respJson = "{name:Sulumbek,\r\nsurname:Habaev}";
                    break;
                }
            }
        } catch (VaymartDataBaseException e) {
            switch (e.getVaymartDataBaseError()) {
                case INTERNAL_ERROR:
                    resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    respJson = "{\"msg\":\""+e.getMessageError()+"\"}";
                    break;
                case STORAGE_ERROR:
                    resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    respJson = "{\"msg\":\""+e.getMessageError()+"\"}";
                    break;
                default:
                    resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                    respJson = "{\"msg\":\"Unimplemented error type\"}";
            }
        }
        resp.setContentType("application/json");
        resp.getWriter().write(respJson + "\n");
        resp.getWriter().flush();

    } catch (IOException e) {
        try {
            resp.getWriter().write(e.getMessage());
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp){
    route(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp){
    route(req, resp);
  }

  @Override
  protected void doHead(HttpServletRequest req, HttpServletResponse resp){
    route(req, resp);
  }

  @Override
  protected void doPut(HttpServletRequest req, HttpServletResponse resp){
    route(req, resp);
  }

  @Override
  protected void doDelete(HttpServletRequest req, HttpServletResponse resp){
    route(req, resp);
  }

  @Override
  protected void doOptions(HttpServletRequest req, HttpServletResponse resp){
    route(req, resp);
  }

  @Override
  protected void doTrace(HttpServletRequest req, HttpServletResponse resp){
    route(req, resp);
  }
}
