package com.team.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.team.dto.manager.Product;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.services.ProductService;
import java.util.ArrayList;
import java.util.HashMap;

public class ProductController {

    private ProductService productService = new ProductService();

    private static Gson GSON = new GsonBuilder().create();

    public void setProductFromJsonInDataBase(String json) {

    }

    public String getAllProductsFromMapToJson() throws VaymartDataBaseException {
        HashMap<String,Product> products = productService.getAllProducts();
        return GSON.toJson(products);
    }

    public String getProductsBySizeFromMapToJson() throws VaymartDataBaseException {
        HashMap<Integer, ArrayList<Product>> products = productService.getProductsBySize();
        return GSON.toJson(products);

    }

}
