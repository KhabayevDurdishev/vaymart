package com.team.controllers;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.team.dto.payment.CustomerEmail;
import com.team.dto.payment.CustomerStripe;
import com.team.dto.payment.StatusPayment;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.services.OrderService;
import com.team.services.PaymentService;

public class PaymentController {

    private static Gson GSON = new GsonBuilder().create();
    private static PaymentService paymentService = new PaymentService();
    private static OrderService orderService = new OrderService();

    public String getEmailByCustomerId(String json) throws VaymartDataBaseException {
        CustomerStripe customerStripe = GSON.fromJson(json, CustomerStripe.class);
        String customerId = customerStripe.getStripeKey();
        String email = paymentService.getEmailByCustomerStripeId(customerId);
        return GSON.toJson(new CustomerEmail(email));
    }


    public void fromJsonToStatus(String json) throws VaymartDataBaseException {
        StatusPayment statusPayment = GSON.fromJson(json, StatusPayment.class);
        int orderCode = Integer.parseInt(statusPayment.getOrderCode());
        String status = statusPayment.getStatus();
        orderService.updateStatusPaymentByOrderCode(orderCode,status);
    }
}
