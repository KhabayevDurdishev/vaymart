package com.team.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.team.dto.manager.ProductCart;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.services.OrderService;

import javax.servlet.http.Cookie;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;


public class OrderController {

    private static Gson GSON = new GsonBuilder().create();
    private OrderService orderService = new OrderService();

    public void fromJsonToProductAndAddProductInOrder(Cookie productsJson, int orderCode) throws UnsupportedEncodingException, VaymartDataBaseException {
        String json = URLDecoder.decode(productsJson.getValue(), "UTF-8");
        ArrayList<ProductCart> carts = GSON.fromJson(json, new TypeToken<ArrayList<ProductCart>>(){}.getType());
        for(ProductCart product : carts){
            orderService.addProductInOrder(product,orderCode);
        }
    }
}
