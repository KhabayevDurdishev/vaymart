package com.team.services;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Customer;
import com.stripe.model.Token;
import com.team.dto.payment.Card;
import com.team.enums.StripeError;
import com.team.exсeptions.VaymartStripeException;
import java.util.HashMap;
import java.util.Map;
public class StripeApiService {

    public String getCustomerIdFromStripe(String email, String token) throws VaymartStripeException {

        Stripe.apiKey = "sk_test_NZxWajazDoCTx898BK7iHZSl";
        Map<String, Object> customerParam = new HashMap<>();
        customerParam.put("email", email);
        try {
            Customer customer = Customer.create(customerParam);
            try {
                Map <String , Object> params = new HashMap<>();
                params.put("source", token);
                customer.getSources().create(params);
                return customer.getId();
            } catch (StripeException e) {
                e.printStackTrace();
                throw new VaymartStripeException(StripeError.CARD_NUMBER_ERROR, "Incorrect token");
            }
        } catch (StripeException e) {
            e.printStackTrace();
            throw new VaymartStripeException(StripeError.EMAIL_ERROR, "Incorrect email");
        }
    }

    public void addCardInCustomer(String customerKey, String token){
        Stripe.apiKey = "sk_test_NZxWajazDoCTx898BK7iHZSl";
        try {
            Customer customer = Customer.retrieve(customerKey);
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("source", token);
            customer.getSources().create(params);
        } catch (StripeException e) {
            e.printStackTrace();
        }
    }

    public Card getStripeCardId(String tokenKey) throws VaymartStripeException {
        try {
            Token token = Token.retrieve(tokenKey);
            String cardId = token.getCard().getId();
            return new Card(cardId);
        } catch (StripeException e) {
            e.printStackTrace();
            throw new VaymartStripeException(StripeError.CARD_NUMBER_ERROR, "Incorrect token");
        }
    }

}
