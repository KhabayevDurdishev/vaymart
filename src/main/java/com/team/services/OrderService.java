package com.team.services;

import com.team.dto.eshop.OrderDTO;
import com.team.dto.eshop.OrderProductDTO;
import com.team.dto.manager.Product;
import com.team.dto.manager.ProductCart;
import com.team.entity.OrderEntity;
import com.team.entity.OrderProductEntity;
import com.team.enums.*;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.exсeptions.VaymartStripeException;
import com.team.repositories.OrderMapper;
import com.team.utils.MyBatisSql;
import org.apache.ibatis.session.SqlSession;
import org.modelmapper.ModelMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class OrderService {
    //Включить ScheduledExecutorService
    private static StripeApiService stripeApiService = new StripeApiService();
    private static ModelMapper modelMapper = new ModelMapper();
    private static ProductService productService = new ProductService();

    public int addOrderInDataBase(String customerId,String amount) throws VaymartDataBaseException, VaymartStripeException {
        long totalAmount = Long.parseLong(amount);
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setCustomerId(Integer.parseInt(customerId));
        orderEntity.setStatus(GlobalStatus.DEFAULT);
        orderEntity.setDate(new Date());
        orderEntity.setTotalAmount(totalAmount);
        orderEntity.setPaymentStatus(PaymentStatus.DEFAULT);
        orderEntity.setWareHouseStatus(WareHouseStatus.DEFAULT);
        orderEntity.setDeliveryStatus(DeliveryStatus.DEFAULT);
        // Узнать и реализовать enum в Data base...
        // Решить проблему Date что бы метод работал для тестера.

        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
            orderMapper.addOrder(orderEntity);
            sqlSession.commit();
            return orderMapper.getOrderId();
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }

    public void updateStatusPaymentByOrderCode(int orderCode, String status) throws VaymartDataBaseException {
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
            orderMapper.updatePaymentStatus(orderCode,status);
            sqlSession.commit();
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Json Format error");
        }
    }

    public void addProductInOrder(ProductCart productCart,int orderCode) throws VaymartDataBaseException {
        int quantity = Integer.parseInt(productCart.getQuantity());
        int productId = Integer.parseInt(productCart.getProductId());
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
            orderMapper.addProductInOrderProducts(orderCode,quantity,productId);
            sqlSession.commit();
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Error in DataBase");
        }
    }

    public OrderDTO getOrderByCustomerId(int customerId) throws VaymartDataBaseException {
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
            OrderEntity orderEntity = orderMapper.getOrderByCustomerId(customerId);
            if(orderEntity!=null) {
                return modelMapper.map(orderEntity, OrderDTO.class);
            }else return new OrderDTO();

        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "There is no order for this customerId.");
        }
    }

    public ArrayList<Product> getProductsIdByOrderCode(int orderCode) throws VaymartDataBaseException {
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            OrderMapper orderMapper = sqlSession.getMapper(OrderMapper.class);
            OrderProductEntity [] orderProductEntity = orderMapper.getProductsByOrderCode(orderCode);
            ArrayList<Product> list = new ArrayList<>();
            for (OrderProductEntity o: orderProductEntity) {
                Product p = productService.getProductsById(o.getProductId());
                list.add(p);
            }
            return list;
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "There is no orderCode for this Products.");
        }
    }
}
