package com.team.services;

import com.team.entity.UserEntity;
import com.team.enums.VaymartDataBaseError;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.utils.Mail;
import com.team.utils.MyBatisSql;
import com.team.dto.eshop.User;
import com.team.repositories.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.modelmapper.ModelMapper;

import javax.mail.MessagingException;
import java.io.IOException;
public class UserService {

    private static ModelMapper modelMapper = new ModelMapper();

    public boolean checkMail(String email, String password) throws VaymartDataBaseException {
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            UserEntity userEntity = userMapper.checkMail(email,password);
            if (userEntity != null && userEntity.isActivation()) {
                return true;
            }
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
        return false;
    }

    public boolean isCheckSession(String email,String password){
        if(email==null || password == null) return false;
        else{
            try {
                SqlSession sqlSession = MyBatisSql.getSqlSession();
                UserEntity userEntity = sqlSession.getMapper(UserMapper.class).checkMail(email,password);
                sqlSession.close();
                return userEntity != null;
            } catch (IOException e) {
                try {
                    throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
                } catch (VaymartDataBaseException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return false;
    }

    public boolean mailCheckForEmployment(String email) throws VaymartDataBaseException {
        try {
            SqlSession sqlSession = MyBatisSql.getSqlSession();
            UserEntity userEntity = sqlSession.getMapper(UserMapper.class).mailCheckForEmployment(email);
            return userEntity == null;
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }

    public void addUsersInDataBase(User user) throws VaymartDataBaseException {
        UserEntity userEntity = modelMapper.map(user,UserEntity.class);
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            Mail.sendMail(user.getEmail(), "\nhttp://localhost:8080/webui/activate?email=" + user.getEmail().toLowerCase() + "&md=" + user.getPassword());
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            userMapper.addUsersInDataBase(userEntity);
            sqlSession.commit();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }

    public void updateActivation(User user) throws VaymartDataBaseException {
        UserEntity userEntity = modelMapper.map(user,UserEntity.class);
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            userMapper.updateActivation(userEntity);
            sqlSession.commit();
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }

    public void addCustomerInUserTable(int customerId, String email, String password) throws VaymartDataBaseException {
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            int userId = userMapper.getUserIdByEmailAndPassword(email,password);
            userMapper.addCustomerInUser(userId,customerId);
            sqlSession.commit();
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }

    public int getLastCustomerId(String email) throws VaymartDataBaseException {
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            int userId = userMapper.getIdByMail(email);
            return userMapper.getCustomerId(userId);
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "This user has no purchases in this store.");
        }
    }
}
