package com.team.services;
import com.team.dto.manager.Product;
import com.team.dto.manager.ProductDTO;
import com.team.entity.MobEntity;
import com.team.entity.ProductEntity;
import com.team.enums.VaymartDataBaseError;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.repositories.ProductMapper;
import com.team.utils.MyBatisSql;
import org.apache.ibatis.session.SqlSession;
import org.modelmapper.ModelMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ProductService {

    private static ModelMapper modelMapper = new ModelMapper();

    public HashMap<String,Product> getAllProducts() throws VaymartDataBaseException {
        HashMap<String,Product> hashMap = new HashMap<>();
        try{
            SqlSession sqlSession = MyBatisSql.getSqlSession();
            ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
            ProductEntity [] productEntities = productMapper.getAllMobilePhone();
            for (ProductEntity p : productEntities){
                Product product = modelMapper.map(p,Product.class);
                product.setProductId(p.getId());
                hashMap.put(product.getVendorCode(),product);
            }
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
        return hashMap;
    }

    public void addProductInDataBase(ProductDTO product) throws VaymartDataBaseException {
        MobEntity productEntity = modelMapper.map(product, MobEntity.class);
        System.out.println(productEntity.toString());
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
            productMapper.addProduct(productEntity);
            sqlSession.commit();
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }

    public HashMap<Integer, ArrayList<Product>> getProductsBySize() throws VaymartDataBaseException {
        int size = 12;
        HashMap<Integer,ArrayList<Product>> hashMap = new HashMap<>();
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
            ProductEntity [] productEntities = productMapper.getAllMobilePhone();
            ArrayList<Product> list = new ArrayList<>();
            for (int i = 0; i<productEntities.length; i++) {
                Product product = modelMapper.map(productEntities[i], Product.class);
                product.setProductId(productEntities[i].getId());
                list.add(product);
                if ((i + 1) == productEntities.length && list.size() != size) {
                    hashMap.put((i + 1) / size, list);
                }else if ((i + 1) % size == 0) {
                    hashMap.put((i + 1) / size, list);
                    list = new ArrayList<>();
                }
            }
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
        return hashMap;
    }

    public Product getProductsById(int productId) throws VaymartDataBaseException {
        try{
            SqlSession sqlSession = MyBatisSql.getSqlSession();
            ProductMapper productMapper = sqlSession.getMapper(ProductMapper.class);
            ProductEntity productEntity = productMapper.getProductById(productId);
            return modelMapper.map(productEntity,Product.class);
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "There is no product for such id");
        }
    }
}
