package com.team.services;

import com.team.dto.eshop.CustomerDTO;
import com.team.dto.payment.Customer;
import com.team.entity.ClientEntity;
import com.team.entity.CustomerEntity;
import com.team.enums.VaymartDataBaseError;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.repositories.CustomerMapper;
import com.team.utils.MyBatisSql;
import org.apache.ibatis.session.SqlSession;
import org.modelmapper.ModelMapper;
import java.io.IOException;

public class CustomerService {

    private static ModelMapper modelMapper = new ModelMapper();
    public int addCustomerInDataBase(CustomerDTO customer) throws VaymartDataBaseException {
        ClientEntity clientEntity = modelMapper.map(customer, ClientEntity.class);
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            CustomerMapper customerMapper = sqlSession.getMapper(CustomerMapper.class);
            customerMapper.insertCustomer(clientEntity);
            sqlSession.commit();
            return customerMapper.getCustomerId();
        } catch (IOException e) {
            e.printStackTrace();
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }

    public Customer getCustomerById(String customerId) throws VaymartDataBaseException {
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            CustomerMapper customerMapper = sqlSession.getMapper(CustomerMapper.class);
            CustomerEntity c = customerMapper.getCustomer(Integer.parseInt(customerId));
            Customer customer = modelMapper.map(c, Customer.class);
            customer.setCustomerKey(c.getId());
            return customer;
        } catch (IOException e) {
            e.printStackTrace();
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }

    public void addCustomerStripeId(int customerId, String stripeKey) throws VaymartDataBaseException {
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            CustomerMapper customerMapper = sqlSession.getMapper(CustomerMapper.class);
            customerMapper.addStripeKeyInCustomerById(customerId,stripeKey);
            sqlSession.commit();
        } catch (IOException e) {
            e.printStackTrace();
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }
    //Доработать проверку созданного клиента!
    public boolean checkForExistenceCustomers(String email,String iin) throws VaymartDataBaseException {
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            CustomerMapper customerMapper = sqlSession.getMapper(CustomerMapper.class);
            CustomerEntity customerEntity = customerMapper.checkForExistence(email,iin);
            return customerEntity != null;

        } catch (IOException e) {
            e.printStackTrace();
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Query error");
        }
    }

    public String getCustomerKeyByIin(String iin) throws VaymartDataBaseException {
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            CustomerMapper customerMapper = sqlSession.getMapper(CustomerMapper.class);
            return customerMapper.getCustomerKey(iin);
        } catch (IOException e) {
            e.printStackTrace();
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "This iin does not exist in the database. Enter the correct iin.");
        }
    }

    public boolean checkingIinInDataBase(String iin) throws VaymartDataBaseException {
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            CustomerMapper customerMapper = sqlSession.getMapper(CustomerMapper.class);
            return customerMapper.checkingIin(iin);
        } catch (IOException e) {
            e.printStackTrace();
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "This iin does not exist in the database. Enter the correct iin.");
        }
    }
}
