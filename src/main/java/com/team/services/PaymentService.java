package com.team.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.team.controllers.PaymentController;
import com.team.dto.payment.Card;
import com.team.dto.payment.Customer;
import com.team.dto.payment.PaymentOrder;
import com.team.dto.payment.Plan;
import com.team.enums.VaymartDataBaseError;
import com.team.enums.VaymartPaymentError;
import com.team.exсeptions.VaymartDataBaseException;
import com.team.exсeptions.VaymartPaymentException;
import com.team.exсeptions.VaymartStripeException;
import com.team.repositories.CustomerMapper;
import com.team.utils.MyBatisSql;
import com.team.utils.Rest;
import org.apache.ibatis.session.SqlSession;

import java.io.IOException;


public class PaymentService {

    private static StripeApiService stripeApiService = new StripeApiService();
    private static PaymentController paymentController = new PaymentController();
    private static Gson GSON = new GsonBuilder().create();

    public void sendOrderInPaymentSystem(PaymentOrder paymentOrder, String token) throws VaymartStripeException, VaymartDataBaseException, VaymartPaymentException {
        Card card = stripeApiService.getStripeCardId(token);
        paymentOrder.getCustomer().setCard(card);

        StringBuilder statusPayment = Rest.restRequest("http://192.168.43.168:8080/api/payment/add", GSON.toJson(paymentOrder));
        if(statusPayment != null) {
            paymentController.fromJsonToStatus(statusPayment.toString());
        }else{
            throw new VaymartPaymentException(VaymartPaymentError.SYSTEM_IS_NOT_RESPONDING,
                "Payment system is not available at the moment, please make payment later.");
        }
    }

    public String getEmailByCustomerStripeId(String customerId) throws VaymartDataBaseException {
        try (SqlSession sqlSession = MyBatisSql.getSqlSession()) {
            CustomerMapper customerMapper = sqlSession.getMapper(CustomerMapper.class);
            return customerMapper.getEmailByCustomerStripeId(customerId);
        } catch (IOException e) {
            throw new VaymartDataBaseException(VaymartDataBaseError.STORAGE_ERROR, "Not Email");
        }
    }
}
