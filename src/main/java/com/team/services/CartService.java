package com.team.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.team.dto.manager.ProductCart;
import org.modelmapper.ModelMapper;
import javax.servlet.http.Cookie;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

public class CartService {

    private static ModelMapper modelMapper = new ModelMapper();
    private static Gson GSON = new GsonBuilder().create();
    // Вынести систему корзины в js

    public Cookie findProducts(Cookie[] cookies){
        Cookie cookie = null;
        for (Cookie cookieTmp : cookies) {
            if (cookieTmp.getName().equals("products")) {
                cookie = cookieTmp;
            }
        }
        return cookie;
    }

    public Cookie createCookieFromJson(Cookie cookie, String vendor, String productId) throws IOException {
        if(cookie == null){
            ArrayList<ProductCart> cartList = new ArrayList<>();
            cartList.add(new ProductCart(vendor,"1",productId));
            String json = GSON.toJson(cartList);
            return new Cookie("products", URLEncoder.encode(json, "UTF-8"));
        }else {
            String json = URLDecoder.decode(cookie.getValue(), "UTF-8");
            ArrayList<ProductCart> carts = GSON.fromJson(json, new TypeToken<ArrayList<ProductCart>>(){}.getType());
            if(vendor != null){
                boolean isSwitch = true;
                for(ProductCart temp : carts){
                    if(temp.getVendorCode().equals(vendor)){
                        isSwitch = false;
                        break;
                    }
                }
                if(isSwitch){
                    carts.add(new ProductCart(vendor, "1",productId));
                    return new Cookie("products",URLEncoder.encode(GSON.toJson(carts), "UTF-8"));
                }
            }
            return new Cookie("products",URLEncoder.encode(GSON.toJson(carts), "UTF-8"));

        }
    }

    public Cookie removeCookie(Cookie cookie,String removeVendor) throws IOException {
        String json = URLDecoder.decode(cookie.getValue(), "UTF-8");
        ArrayList<ProductCart> carts = GSON.fromJson(json, new TypeToken<ArrayList<ProductCart>>(){}.getType());
        for (int i = 0; i<carts.size(); i++) {
            ProductCart cart = carts.get(i);
            if (cart.getVendorCode().equals(removeVendor)) {
                carts.remove(i);
                break;
            }
        }
        return new Cookie("products",URLEncoder.encode(GSON.toJson(carts), "UTF-8"));
    }
    public Cookie removeAll(Cookie [] cookies){
        Cookie cookie = findProducts(cookies);
        cookie.setMaxAge(-1);
        return cookie;
    }

    public void setQuantity(String [] quantityMobilePhone, Cookie cookie){
        for(String string : quantityMobilePhone){
        }
    }
}
