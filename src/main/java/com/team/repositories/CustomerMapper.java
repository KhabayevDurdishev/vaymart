package com.team.repositories;

import com.team.entity.ClientEntity;
import com.team.entity.CustomerEntity;
import org.apache.ibatis.annotations.Param;

public interface CustomerMapper {
    CustomerEntity getCustomer(@Param("customerId")int customerId);
    int getCustomerId();
    String getEmailByCustomerStripeId(@Param("customerStripeId") String customerStripeId);
    void addStripeKeyInCustomerById(@Param("id") int id, @Param("stripeKey") String stripeKey);
    CustomerEntity checkForExistence(@Param("email") String email, @Param("iin") String iin);
    void insertCustomer(ClientEntity clientEntity);
    String getCustomerKey(@Param("iin") String iin);
    boolean checkingIin(@Param("iin") String iin);
}
