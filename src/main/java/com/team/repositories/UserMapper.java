package com.team.repositories;

import com.team.entity.UserEntity;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    UserEntity checkMail(@Param("email")String email,@Param("password") String password);
    void addUsersInDataBase(UserEntity userEntity);
    void updateActivation(UserEntity userEntity);
    UserEntity mailCheckForEmployment(@Param("email")String email);
    void addCustomerInUser(@Param("userId") int userId, @Param("customerId") int customerId);
    int getUserIdByEmailAndPassword(@Param("email") String email, @Param("password") String password);
    int getIdByMail(@Param("email") String email);
    int getCustomerId(@Param("userId") int userId);
}
