package com.team.repositories;

import com.team.dto.manager.Manager;
import com.team.entity.ManagerEntity;
import org.apache.ibatis.annotations.Param;

public interface ManagerMapper {
    void addManager(Manager manager);
    ManagerEntity getManager(@Param("email") String email, @Param("password") String password);
}
