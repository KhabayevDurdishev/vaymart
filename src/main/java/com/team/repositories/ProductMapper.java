package com.team.repositories;
import com.team.entity.MobEntity;
import com.team.entity.ProductEntity;
import org.apache.ibatis.annotations.Param;

public interface ProductMapper {
    ProductEntity[] getAllMobilePhone();
    void addProduct(MobEntity productEntity);
    ProductEntity getProductById(@Param("productId") int productId);
}
