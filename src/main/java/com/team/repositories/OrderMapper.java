package com.team.repositories;


import com.team.entity.OrderEntity;
import com.team.entity.OrderProductEntity;
import org.apache.ibatis.annotations.Param;

public interface OrderMapper {
    void addOrder(OrderEntity orderEntity);
    int getOrderId();
    void updatePaymentStatus(@Param("orderCode") int orderCode, @Param("status") String status);
    void addProductInOrderProducts(@Param("orderCode") int orderCode,@Param("quantity") int quantity,@Param("productId") int productId);
    OrderEntity getOrderByCustomerId(@Param("customerId") int customerId);
    OrderProductEntity[] getProductsByOrderCode(@Param("orderCode") int orderCode);
}
