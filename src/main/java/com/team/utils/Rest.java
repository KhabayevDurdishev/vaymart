package com.team.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Rest {

    public static StringBuilder restRequest(String url, String json){
        try {

            URL obj = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) obj.openConnection();

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            DataOutputStream writer = new DataOutputStream(connection.getOutputStream());
            writer.writeBytes(json);
            writer.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder respJson = new StringBuilder();
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                respJson.append(inputLine);
            }

            writer.close();
            reader.close();

            return respJson;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
