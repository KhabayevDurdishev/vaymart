package com.team.filters;

import com.team.exсeptions.VaymartDataBaseException;
import com.team.services.ProductService;
import com.team.services.UserService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/webui/account/*")
public class FilterAccountServlet implements Filter {
    private static UserService userService = new UserService();
    private static ProductService productService = new ProductService();
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession(true);
        String path = req.getRequestURI();

        String email = (String) session.getAttribute("email");
        String password = (String) session.getAttribute("password");
        try {
            boolean tmpBoolean = false;
            if(email!=null && password != null) {
                tmpBoolean = userService.checkMail(email.toLowerCase(), password);
            }
            req.setAttribute("session", tmpBoolean);
            if (tmpBoolean) {
                switch (path) {
                    case "/webui/account/": {
                        chain.doFilter(req,resp);
                        break;
                    }
                    case "/webui/account/installment-payment": {
                        chain.doFilter(req,resp);
                        break;
                    }
                    case "/webui/account/user-information": {
                        chain.doFilter(req,resp);
                        break;
                    }
                    case "/webui/account/history": {
                        chain.doFilter(req,resp);
                        break;
                    }
                    case "/webui/account/cancel-payment": {
                        chain.doFilter(req,resp);
                        break;
                    }
                    default:{
                        req.setAttribute("allProducts", productService.getAllProducts());
                        req.getRequestDispatcher("/web/index.jsp").forward(req,resp);
                    }
                }
            }else{
                req.setAttribute("allProducts", productService.getAllProducts());
                req.getRequestDispatcher("/web/index.jsp").forward(req,resp);
            }

        } catch (VaymartDataBaseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void destroy() {

    }
}
