package com.team.filters;

import com.team.exсeptions.VaymartDataBaseException;
import com.team.services.UserService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/webui/*")
public class FilterSessionServlet implements Filter {

    private static UserService userService = new UserService();


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession(true);

        String email = (String) session.getAttribute("email");
        String password = (String) session.getAttribute("password");

        boolean tmpBoolean = false;
        if(email!=null && password != null) {
            try {
                tmpBoolean = userService.checkMail(email.toLowerCase(), password);
            } catch (VaymartDataBaseException e) {
                e.printStackTrace();
            }
        }
        req.setAttribute("session", tmpBoolean);
        chain.doFilter(req,resp);

    }

    @Override
    public void destroy() {

    }
}
