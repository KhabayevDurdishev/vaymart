package com.team.filters;

import com.team.exсeptions.VaymartDataBaseException;
import com.team.services.UserService;

import javax.ejb.EJB;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
@WebFilter("/webui/*")
public class FilterMainServlet implements Filter {

    private UserService userService = new UserService();

    public void init(FilterConfig filterConfig){

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain){
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        HttpSession session = req.getSession(true);
        String path = req.getRequestURI();

        try {
            String email = (String) session.getAttribute("email");
            String password = (String) session.getAttribute("password");
            if (email != null && password != null && userService.checkMail(email.toLowerCase(), password)) {
                switch (path){
                    case "/webui/login" :{
                        resp.sendRedirect("/webui/index");
                        return;
                    }
                    case "/webui/signup" :{
                        resp.sendRedirect("/webui/index");
                        return;
                    }
                    case "/webui/" :{
                        resp.sendRedirect("/webui/index");
                        return;
                    }
                }
            }
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (ServletException | IOException e) {
            try {
                resp.getWriter().write(e.getMessage());
            } catch (IOException exc) {
                exc.printStackTrace();
            }
        } catch (VaymartDataBaseException e) {
            String error = "{\"msg\":\""+e.getMessageError()+"\"}";
            try {
                resp.getWriter().write(error);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void destroy() {

    }
}
