package com.team.enums;

public enum VaymartDataBaseError {
    STORAGE_ERROR, INTERNAL_ERROR
}
