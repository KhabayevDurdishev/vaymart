package com.team.entity;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class CustomerEntity{
    private int id;
    private String name;
    private String surname;
    private String iin;
    private String email;
    private String telephone;
    private String city;
    private String street;
    private String house;
    private String flat;
    private String stripeKey;
}


