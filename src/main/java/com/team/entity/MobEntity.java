package com.team.entity;

import com.team.dto.manager.MobilePhone;
import com.team.dto.manager.ShopWindow;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

//Доработать систему пополнения товара!
public class MobEntity {
    private MobilePhone phone;
    private String link;
    private ShopWindow shop;
}
