package com.team.entity;

import com.team.dto.eshop.IdentificationData;
import com.team.dto.eshop.Location;
import com.team.dto.eshop.Person;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientEntity {
    private Person person;
    private IdentificationData idData;
    private Location location;
}
