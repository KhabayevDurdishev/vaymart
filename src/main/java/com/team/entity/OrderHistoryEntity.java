package com.team.entity;
import lombok.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderHistoryEntity {
    private int id;
    private int customerId;
    private int orderId;
    private String status;
    private Date dateFrom;
    private Date dateTill;
}
