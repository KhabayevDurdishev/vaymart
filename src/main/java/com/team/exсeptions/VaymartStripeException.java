package com.team.exсeptions;

import com.team.enums.StripeError;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class VaymartStripeException extends Throwable {
    private StripeError stripeError;
    private String message;

    public VaymartStripeException(StripeError stripeError, String message) {
        this.stripeError = stripeError;
        this.message = message;
    }

    public VaymartStripeException(StripeError stripeError) {
        this.stripeError = stripeError;
    }

}
