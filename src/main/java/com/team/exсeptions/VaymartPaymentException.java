package com.team.exсeptions;

import com.team.enums.VaymartPaymentError;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VaymartPaymentException extends Throwable{
    VaymartPaymentError vaymartPaymentError;
    String message;

    public VaymartPaymentException(VaymartPaymentError vaymartPaymentError, String message) {
        this.vaymartPaymentError = vaymartPaymentError;
        this.message = message;
    }

}
