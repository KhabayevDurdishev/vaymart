package com.team.exсeptions;

import com.team.enums.VaymartDataBaseError;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VaymartDataBaseException extends Throwable{
    private VaymartDataBaseError vaymartDataBaseError;
    private String messageError;

    public VaymartDataBaseException(VaymartDataBaseError vaymartDataBaseError) {
        this.vaymartDataBaseError = vaymartDataBaseError;
    }

    public VaymartDataBaseException(VaymartDataBaseError vaymartDataBaseError, String messageError) {
        this.vaymartDataBaseError = vaymartDataBaseError;
        this.messageError = messageError;
    }
}
