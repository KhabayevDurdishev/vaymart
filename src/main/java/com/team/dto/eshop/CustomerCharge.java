package com.team.dto.eshop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class CustomerCharge {
    private String name;
    private String surname;
    private String city;
    private String street;
    private String houseNum;
    private String flatNum;
    private String email;
    private String tel;
    private String iin;
    private String payment;
}
