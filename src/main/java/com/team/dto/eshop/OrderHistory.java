package com.team.dto.eshop;

import lombok.*;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderHistory {
    private int customerId;
    private int orderId;
    private String status;
    private Date dateFrom;
    private Date dateTill;
}
