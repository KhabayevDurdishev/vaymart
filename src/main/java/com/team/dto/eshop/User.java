package com.team.dto.eshop;

import lombok.*;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class User {
    private String email;
    private String password;
    private boolean activation;

}
