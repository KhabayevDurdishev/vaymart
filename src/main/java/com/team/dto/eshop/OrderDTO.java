package com.team.dto.eshop;

import com.team.enums.DeliveryStatus;
import com.team.enums.GlobalStatus;
import com.team.enums.PaymentStatus;
import com.team.enums.WareHouseStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderDTO {
    private int orderCode;
    private Date date;
    private long totalAmount;
    private GlobalStatus status;
    private PaymentStatus paymentStatus;
    private WareHouseStatus wareHouseStatus;
    private DeliveryStatus deliveryStatus;
}
