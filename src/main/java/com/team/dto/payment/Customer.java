package com.team.dto.payment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Customer {
    private int customerKey;
    private String name;
    private String surname;
    private String iin;
    private String email;
    private String telephone;
    private String city;
    private String street;
    private String house;
    private String flat;
    private String stripeKey;
    private Card card;
}
