package com.team.dto.payment;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class StripeCharge {
    private String customerId;
    private String token;
    private String amount;
    private String plan;
    private String month;

}
