package com.team.dto.payment;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PaymentOrder {
    private int orderCode;
    private int totalAmount;
    private Customer customer;
    private Plan plan;
}
