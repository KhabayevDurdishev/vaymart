package com.team.dto.manager;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductCart {
    private String vendorCode;
    private String quantity;
    private String productId;
}
