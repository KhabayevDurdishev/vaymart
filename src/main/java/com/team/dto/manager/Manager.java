package com.team.dto.manager;


import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Manager {
    private String email;
    private String password;
}
