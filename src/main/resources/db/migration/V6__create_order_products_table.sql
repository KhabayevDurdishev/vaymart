-- auto-generated definition
create table order_products
(
  id         serial not null
    constraint order_products_pkey
    primary key,
  order_id   integer
    constraint order_products_order_id_fkey
    references orders,
  product_id integer
    constraint order_products_products_id___fk
    references products,
  quantity   integer
);