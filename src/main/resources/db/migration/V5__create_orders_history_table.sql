-- auto-generated definition
create table orders_history
(
  id          serial not null
    constraint orders_history_pkey
    primary key,
  customer_id integer
    constraint orders_history_customer_id_fkey
    references customers,
  order_id    integer
    constraint orders_history_order_id_fkey
    references orders,
  status      varchar(100),
  date_from   timestamp,
  date_till   timestamp
);