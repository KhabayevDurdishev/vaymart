-- auto-generated definition
create table users
(
  id          serial not null
    constraint users_pkey
    primary key,
  customer_id integer
    constraint users_customer_id_fkey
    references customers,
  email       varchar(100),
  password    varchar(100),
  activation  boolean
);