-- auto-generated definition
create table manager
(
  id       serial not null
    constraint manager_pkey
    primary key,
  email    varchar(100),
  password varchar(100)
);