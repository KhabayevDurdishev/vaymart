-- auto-generated definition
create table products
(
  id          serial not null
    constraint products_pkey
    primary key,
  brand       varchar(100),
  model       varchar(100),
  color       varchar(100),
  memory_size integer,
  link        varchar(100),
  vendor_code varchar(100),
  price       integer,
  quantity    integer
);
