-- auto-generated definition
create table orders
(
  order_code       serial not null
    constraint orders_pkey
    primary key,
  customer_id      integer
    constraint orders_customer_id_fkey
    references customers,
  status           varchar(100),
  total_amount     integer,
  initial_amount   integer,
  month            integer,
  payment_status   varchar(100),
  warehouse_status varchar(100),
  delivery_status  varchar(100),
  date             timestamp
);