-- auto-generated definition
create table user_customer
(
  user_id     integer
    constraint user_customers_users_id_fk
    references users,
  customer_id integer
    constraint user_customers_customers_id_fk
    references customers
);

alter table user_customer
  owner to postgres;

