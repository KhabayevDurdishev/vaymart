-- auto-generated definition
create table reserve_product
(
  order_id           integer
    constraint reserve_order_id___fk
    references orders,
  product_id         integer
    constraint reserve_product_id__fk
    references products,
  quantity_reserve   integer,
  reserve_start_date timestamp,
  reserve_out_date   timestamp
);